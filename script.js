var currentFileName = "", interval;

$(function () {
    $("#startBtn").click(function () {
        currentFileName = Date.now();
        resetLinks();
        interval = setInterval(checkTweetsAmount, 3000);
        $.ajax({
            type: "post",
            url: "ajax.php",
            data: {"start_streaming": true, query: $("#keywords").val(), filename: currentFileName},
            dataType: "json",
            success: function (result) {
                console.log(result);
            }
        })
    });


    $("#stopBtn").click(function () {
        clearInterval(interval);
        $.ajax({
            type: "post",
            url: "ajax.php",
            data: {"stop_streaming": true, query: $("#keywords").val()},
            dataType: "json",
            success: function (result) {
                console.log(result);
            }
        });
    });


});

function checkTweetsAmount() {
    $.ajax({
        type: "post",
        url: "ajax.php",
        data: {"check_file_size": true, filename: currentFileName},
        success: function (result) {
            var amount = parseInt(result);

            if (!isNaN(amount)) {
                if (amount > 0) renderLinks();
                $("#tweet_count").html(parseInt(result));
            }

        }
    })
}

function resetLinks() {
    $(".links a").each(function () {
        $(this).attr({href: ""});
    });
    $(".links").hide();
}

function renderLinks() {
    $(".links a").each(function () {
        $(this).attr({href: "data/" + currentFileName + "_" + $(this).attr("data-link") + ".txt"});
    });
    $(".links").show();
}



