<?php file_put_contents( "flag", "0" ); ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="style.css">

	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<script src="script.js"></script>

	<title>Get Tweets</title>
</head>
<body>
<div id="searchWrapper">
	<p>
		<label for="keywords">Keywords</label>
		<textarea name="keywords" id="keywords" cols="5" rows="3" class="form-control" placeholder="Enter keywords"></textarea>
	</p>

	<div class="row">
		<div class="col-lg-6">
			<div class="input-group">
				  <span class="input-group-btn">
					<button class="btn btn-default" type="button" id="startBtn">START</button>
					<button class="btn btn-default" type="button" id="stopBtn">STOP</button>
				  </span>
			</div>
		</div>
		<h3>Tweets count: <span class="label label-default" id="tweet_count">0</span></h3>

		<div class="links">
			<a href="" data-link="names" target="_blank">names</a>
			<a href="" data-link="messages" target="_blank">messages</a>
			<a href="" data-link="coordinates" target="_blank">locations</a>
			<a href="" data-link="timestamp" target="_blank">time</a>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="table-responsive">
			<h4>Tweets amount: <span id="tweetsAmount"></span></h4>

			<p id="linkP"><span id="fileLink"></span><span id="friendsLink"></span><span id="screenNamesLink"></span></p>

			<div class="progress">
				<div id="progress-bar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
			</div>
			<table class="table table-hover" id="resultTable">
			</table>
		</div>
		<div class="col-md-12 text-center">
			<ul class="pagination pagination-lg pager" id="myPager"></ul>
		</div>
	</div>
</div>
</body>
</html>