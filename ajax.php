<?php

set_time_limit( 9999999 );

require_once( 'lib/Phirehose.php' );
require_once( 'lib/OauthPhirehose.php' );

define( "TWITTER_CONSUMER_KEY", "nJOsCfXq2ZaYGqxwIUBaA" );
define( "TWITTER_CONSUMER_SECRET", "RDd4pZCnoLeFlJcBIi2HnyalG7BA80nZjvviJpcA" );

define( "OAUTH_TOKEN", "743251615-SzXiqIlKMN4A14U2iGKuUmA32llHkVjSh8UAEAQL" );
define( "OAUTH_SECRET", "HsOb2TZtoYXWbMg5Et2OoTIH8iPJsu128AGIHcVRvtWJD" );

define( "FLAG_FILE", "flag" );

class Tracking extends OauthPhirehose {

	public $filename;
	public $_keywords;

	public function enqueueStatus( $status ) {
		$data = json_decode( $status, true );
		if ( isRunning() ) {

			if ( $data["user"]["geo_enabled"] == "1" ) {
				$username      = $data["user"]["screen_name"];
				$tweet_message = $data["text"];
				$coord         = $data["geo"]["coordinates"][0] . ";" . $data["geo"]["coordinates"][1];
				$created_at    = $data["timestamp_ms"];

				file_put_contents( "data/cc.txt", $data["place"]["country_code"] . ", ", FILE_APPEND );


				if ( count( $this->_keywords ) > 0 && $this->_keywords[0] != "" ) {
					$fits = false;
					foreach ( $this->_keywords as $value ) {
						if ( strpos( $tweet_message, $value ) !== false ) {
							$fits = true;
						}
					}
					if ( ! $fits ) {
						return false;
					}
				}


				touch( $this->filename . "_names.txt" );
				touch( $this->filename . "_messages.txt" );
				touch( $this->filename . "_coordinates.txt" );
				touch( $this->filename . "_timestamp.txt" );

				$usernames  = file_get_contents( $this->filename . "_names.txt" ) != "" ? json_decode( file_get_contents( $this->filename . "_names.txt" ) ) : [ ];
				$messages   = file_get_contents( $this->filename . "_messages.txt" ) != "" ? json_decode( file_get_contents( $this->filename . "_messages.txt" ) ) : [ ];
				$coords     = file_get_contents( $this->filename . "_coordinates.txt" ) != "" ? json_decode( file_get_contents( $this->filename . "_coordinates.txt" ) ) : [ ];
				$timestamps = file_get_contents( $this->filename . "_timestamp.txt" ) != "" ? json_decode( file_get_contents( $this->filename . "_timestamp.txt" ) ) : [ ];


				$usernames[]  = $username;
				$messages[]   = $tweet_message;
				$coords[]     = $coord;
				$timestamps[] = $created_at;

				file_put_contents( $this->filename . "_names.txt", json_encode( $usernames ) );
				file_put_contents( $this->filename . "_messages.txt", json_encode( $messages ) );
				file_put_contents( $this->filename . "_coordinates.txt", json_encode( $coords ) );
				file_put_contents( $this->filename . "_timestamp.txt", json_encode( $timestamps ) );

			}
		} else {
			$this->disconnect();
			die( "done" );
		}
	}
}


if ( $_POST ) {

	if ( isset( $_POST["start_streaming"] ) ) {
		$sc    = new Tracking( OAUTH_TOKEN, OAUTH_SECRET, Phirehose::METHOD_FILTER );
		$track = explode( ",", str_replace( "\n", "", str_replace( " ", "", $_POST["query"] ) ) );

		if ( ! isRunning() ) {
			changeFlag( "1" );

			$sc->setLocations( array(
				array( 115.10, - 35.15, 152.20, - 13.58 ), // Australia
			) );
			$sc->_keywords = $track;
			$sc->filename  = "data/{$_POST["filename"]}";

			$sc->consume();
		}

	} else if ( isset( $_POST["stop_streaming"] ) ) {
		changeFlag( "0" );
	} else if ( isset( $_POST["check_file_size"] ) ) {
		$filename = $_POST["filename"];
		$data     = array();
		if ( file_exists( "data/{$_POST["filename"]}_timestamp.txt" ) ) {
			$data = json_decode( file_get_contents( "data/{$_POST["filename"]}_timestamp.txt" ) );
		}
		echo count( $data );
	}
}

function isRunning() {
	return (bool) file_get_contents( FLAG_FILE );
}

function changeFlag( $val ) {
	file_put_contents( FLAG_FILE, $val );
}